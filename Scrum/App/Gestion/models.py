from django.db import models
from django.contrib.auth import get_user_model

# Create your models here.

class Rol(models.Model):
    id_rol = models.CharField(max_length=12, primary_key=True)
    descripcion = models.CharField(max_length=30)

    def __str__(self):
        return self.id_rol + ',' + self.descripcion

# el UsuarioRol el usuario puede tener muchos roles
# un rol puede tener muchos usuarios
# class UsuarioRol(models.Model):
#	usuario=models.ForeignKey(get_user_model(),on_delete=models.CASCADE)
#	codRol=models.ForeignKey(Rol,on_delete=models.CASCADE)
#	def __str__(self):
#		return str(self.usuario)+','+str(self.codRol)

class Usuario(models.Model):
    codigo = models.CharField(max_length=12, primary_key=True)
    #usuario = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, related_name='userRol')
    rol = models.ForeignKey(Rol, on_delete=models.CASCADE, related_name='rolUser')

    def __str__(self):
        return str(self.rol)

#el UsuarioRol el usuario puede tener muchos roles
#un rol puede tener muchos usuarios
#class UsuarioRol(models.Model):
#	usuario=models.ForeignKey(get_user_model(),on_delete=models.CASCADE)
#	codRol=models.ForeignKey(Rol,on_delete=models.CASCADE)
#	def __str__(self):
#		return str(self.usuario)+','+str(self.codRol)
